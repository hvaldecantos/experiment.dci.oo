library(colorspace)
# counts <- table(mtcars$vs, mtcars$gear)
#
# barplot(counts, main="Car Distribution by Gears and VS",
#         xlab="Number of Gears", col=c("darkblue","red"),
#         legend = rownames(counts))
#
# counts <- table(mtcars$vs, mtcars$gear)
# barplot(counts, main="Car Distribution by Gears and VS",
#         xlab="Number of Gears", col=c("darkblue","red"),
#         legend = rownames(counts), beside=TRUE)

# sysname = 'library'
# sourcecode <- read.csv("data/exp_source_code_data.csv", header=TRUE, stringsAsFactors = F)
# scdata <- sourcecode[sourcecode$system==sysname,]
#
# borrowlibit <- data.frame(matrix(0L, nrow = 1, ncol = 3, dimnames = list(NULL, c('step', 'cor', 'pvalue')))) # starts with zeros
# barplot(scdata$sloc ~ scdata$factor * scdata$system, col=c("gray","white"))

# cols <- heat.colors(20, alpha = 0.8)[c(2,6,6,6,9)]
cols <- heat.colors(40, alpha = 0.8)[c(5,11,11,11,17)]

#################################################

# library total 137 SLOC (roles 5), total system 189
dcilibrary <- matrix(0L, nrow = 5, ncol = 1, dimnames = list(c('code', 'trigger', 'clause', 'boiler', 'rest'), c('BorrowLibraryItem\n(DCI context)')))
# dcilibrary['rest',] <- 52 # 189 - 137
# dcilibrary['boiler',] <- 10
# dcilibrary['clause',] <- 11
# dcilibrary['trigger',] <- 3
# dcilibrary['code',] <- 113 # 137 - 10 - 11
dcilibrary['rest',] <- 52 # 189 - 137
dcilibrary['boiler',] <- 24
dcilibrary['clause',] <- 0
dcilibrary['trigger',] <- 0
dcilibrary['code',] <- 113 # 137 - 10 - 11

# library total 37 SLOC, total system 195 (the most important is not the larger pc)
oolibrary <- matrix(0L, nrow = 5, ncol = 1, dimnames = list(c('code', 'trigger', 'clause', 'boiler', 'rest'), c('Main\n(OO class)')))
oolibrary['rest',] <- 158
oolibrary['boiler',] <- 0
oolibrary['clause',] <- 0
oolibrary['trigger',] <- 0
oolibrary['code',] <- 37

ds = cbind(oolibrary, dcilibrary)
# par(mar=c(4.5,2,1,.5), mfrow=c(1,3))
par(mar=c(7,4,2.5,.5), mfrow=c(1,3))
# barplot(ds, col=colors()[c(23,23, 89,12,45)], border="white", space=0.04, font.axis=1, xlab="Library", ylim= c(0,230), cex.lab = 1.4, mgp=c(2.2, .7, 0))
# barplot(ds, col=cols, border="white", space=0.04, font.axis=1, xlab="Library", ylim= c(0,230), cex.lab = 1.4, mgp=c(2.6, 1.1, 0))
barplot(ds, ylim=c(0,250), col=cols, border="white", space=0.04, cex.axis=2, cex.names=1.8, font.axis=1, xlab="Library", cex.lab = 2.4, mgp=c(5, 2.3, 0))

#################################################
# menu total 116 SLOC (4 roles) - total system 224
dcimenu <- matrix(0L, nrow = 5, ncol = 1, dimnames = list(c('code', 'trigger', 'clause', 'boiler', 'rest'), c('MenuInterface\n(DCI context)')))
# dcimenu['rest',] <- 108
# dcimenu['boiler',] <- 8
# dcimenu['clause',] <- 12
# dcimenu['trigger',] <- 3
# dcimenu['code',] <- 93
dcimenu['rest',] <- 108
dcimenu['boiler',] <- 23
dcimenu['clause',] <- 0
dcimenu['trigger',] <- 0
dcimenu['code',] <- 93

# total class 97 ,total system 228
oomenu <- matrix(0L, nrow = 5, ncol = 1, dimnames = list(c('code', 'trigger', 'clause', 'boiler', 'rest'), c('MenuStructure\n(OO class)')))
oomenu['rest',] <- 131
oomenu['boiler',] <- 0
oomenu['clause',] <- 0
oomenu['trigger',] <- 0
oomenu['code',] <- 97

ds = cbind(oomenu, dcimenu)
par(new=F)
# barplot(ds, col=cols, border="white", space=0.04, font.axis=1, xlab="Menu", ylim= c(0,230), cex.lab = 1.4, mgp=c(2.6, 1.1, 0))
barplot(ds, ylim=c(0,250), col=cols, border="white", space=0.04, cex.axis=2, cex.names=1.8, font.axis=1, xlab="Menu", cex.lab = 2.4, mgp=c(5, 2.3, 0))
# #################################################

# spell total 136 SLOC (4 roles) - total system 223
dcispell1 <- matrix(0L, nrow = 5, ncol = 1, dimnames = list(c('code', 'trigger', 'clause', 'boiler', 'rest'), c('SpellChecker\n(DCI context)')))
# dcispell1['rest',] <- 87
# dcispell1['boiler',] <- 8
# dcispell1['clause',] <- 7
# dcispell1['trigger',] <- 3
# dcispell1['code',] <- 118
dcispell1['rest',] <- 87
dcispell1['boiler',] <- 18
dcispell1['clause',] <- 0
dcispell1['trigger',] <- 0
dcispell1['code',] <- 118

# total class 73 ,total system 237
oospell1 <- matrix(0L, nrow = 5, ncol = 1, dimnames = list(c('code', 'trigger', 'clause', 'boiler', 'rest'), c('SpellChecker\n(OO class)')))
oospell1['rest',] <- 164
oospell1['boiler',] <- 0
oospell1['clause',] <- 0
oospell1['trigger',] <- 0
oospell1['code',] <- 73

ds = cbind(oospell1, dcispell1)
par(new=F)
# barplot(ds, col=cols, border="white", space=0.04, cex.axis=1.4, font.axis=1, xlab="Spell checker", cex.lab = 1.4, mgp=c(2.6, 1.1, 0))
barplot(ds, ylim=c(0,250), col=cols, border="white", space=0.04, cex.axis=2, cex.names=1.8, font.axis=1, xlab="Spell checker", cex.lab = 2.4, mgp=c(5, 2.3, 0))
